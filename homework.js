/************************
Vasily Svitkin 2017
Netology NodeJS course
Home work 1

Пункты задания помечены звёздочкой в нечале комментария (//*)
*/

//*Создать класс Pokemon, конструктор которого принимает имя и уровень в качестве аргумента. Все экземпляры этого класса должны иметь общий метод show, который выводит информацию о покемоне.
// Класс покемона
class Pokemon {
  constructor (name, power) {
    this.name=name;
    this.power=power;
  }
  show(){
    console.log(`Hello, I'm pokemon ${this.name}, my power is ${this.power}`);
  }
  //*Переопределите и используйте метод valueOf у покемонов, для решения этой задачи.
  valueOf(){
    return this.power;
  }
}

//*Создать класс PokemonList, который в качестве аргументов принимает любое количество покемонов. Экземпляры этого класса должны обладать всеми функциями массива. А так же иметь метод add, который принимает в качестве аргументов имя и уровень, создает нового покемона и добавляет его в список.
// Класс списка покемонов

//PokemonList.

class PokemonList extends Array{
  constructor (...pokemons){
    super();
    pokemons.forEach(p=>{this.push(p)});
  }
  add(name, power){
    this.push(new Pokemon(name, power));
  }
  //*Добавить спискам покемонов метод show, который выводит информацию о покемонах и их общее количество в списке.
  show(){
    console.log('Count of pokemons in list : '+this.length);
    this.forEach(p=>{p.show();});
  }
  //*Добавить спискам покемонов метод max, который возвращает покемона максимального уровня.
  max(){
    let pokemon=null;;
    this.forEach(p=>{
      if ((pokemon==null)||(p>pokemon)){
        pokemon=p;
      }});
    return pokemon;
  }
}

//*Создать два списка покемонов и сохранить их в переменных lost и found. Имена и уровни придумайте самостоятельно.
// Два тестовых покемона - для добавления к списку
let fish = new Pokemon('Fish', 10);
let dog = new Pokemon('Dog',3);
//Список покемонов, в конструкторе добавляем сразу двух покемонов

let lost = new PokemonList(fish, dog);
let found = new PokemonList();

//*Добавить несколько новых покемонов в каждый список.
lost.add('Bird',7);
lost.add('Turtle',4);
found.add('Cat',2);
found.add('Bear',9);

//*Перевести одного из покемонов из списка lost в список found
found.push(lost.pop());

//Выводим список покемонов
console.log('Lost list:');
lost.show();
console.log('Found list:');
found.show();
console.log('Best in lost:');
lost.max().show();
console.log('Best in found:');
found.max().show();
